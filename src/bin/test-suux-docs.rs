use mktemp::Temp;
use std::fs::{self, File};
use std::io::{self, BufRead, Write};
use std::process::{Command, Stdio};

enum State {
    OutsideCodeBlock,
    StartingCodeBlock,
    InsideExample(Example),
    InsideOther,
}

#[derive(Clone, Debug)]
struct Example {
    command: String,
    output: Vec<String>,
}

impl Example {
    fn new(command: &str) -> Self {
        Self {
            command: String::from(command),
            output: Vec::new(),
        }
    }

    fn add_output(&mut self, line: String) {
        self.output.push(line);
    }
}

fn main() -> Result<(), io::Error> {
    let mut temp_dir = None;
    for example in find_examples("README.md")? {
        if example.command == "list ." {
            temp_dir = Some(create_tmpdir(example.output)?)
        } else {
            if let Some(ref temp_dir) = temp_dir {
                check_command(temp_dir, example)?;
            } else {
                return Err(io::Error::new(
                    io::ErrorKind::Other,
                    format!(
                        "Command {} run without list . first.",
                        example.command
                    ),
                ));
            }
        }
    }
    Ok(())
}

fn check_command(temp_dir: &Temp, example: Example) -> Result<(), io::Error> {
    println!("Testing command: {}", example.command);

    let mut child = Command::new("bash")
        .current_dir(temp_dir.as_path())
        .stdin(Stdio::piped())
        .stderr(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()?;

    child
        .stdin
        .as_mut()
        .ok_or(io::Error::new(
            io::ErrorKind::Other,
            "Child process stdin has not been captured!",
        ))?
        .write_all(format!("{}\n", example.command).as_bytes())?;

    let output = child.wait_with_output()?;

    if output.status.success() {
        assert_eq!(
            output
                .stdout
                .lines()
                .map(|ln| ln
                    .unwrap_or(String::from("<<Unable to decode utf8>>")))
                .collect::<Vec<String>>(),
            example.output
        );
        Ok(())
    } else {
        Err(io::Error::new(
            io::ErrorKind::Other,
            format!(
                "Command failed:\n {}",
                String::from_utf8(output.stderr)
                    .unwrap_or(String::from("<<Unable to decode stderr>>"))
            ),
        ))
    }
}

fn create_tmpdir(output: Vec<String>) -> Result<Temp, io::Error> {
    let ret = Temp::new_dir()?;
    for line in output {
        if let Some(last_slash_pos) = line.rfind("/") {
            fs::create_dir_all(ret.as_path().join(&line[..last_slash_pos]))?;
            if last_slash_pos != line.len() - 1 {
                File::create(ret.as_path().join(line))?;
            }
        } else if line == "src" {
            // TODO: hard-coded what is a dir!
            fs::create_dir(ret.as_path().join(line))?;
        } else {
            File::create(ret.as_path().join(line))?;
        }
    }
    Ok(ret)
}

fn find_examples(filename: &str) -> Result<Vec<Example>, io::Error> {
    let mut ret = Vec::new();
    let mut state = State::OutsideCodeBlock;

    for line in io::BufReader::new(File::open(filename)?).lines() {
        match line {
            Ok(line) => match state {
                State::OutsideCodeBlock => {
                    if line == "```bash" {
                        state = State::StartingCodeBlock;
                    }
                }
                State::StartingCodeBlock => {
                    if line.starts_with("$ ") {
                        state = State::InsideExample(Example::new(&line[2..]));
                    } else {
                        state = State::InsideOther;
                    }
                }
                State::InsideExample(ref mut example) => {
                    if line == "```" {
                        ret.push(example.clone());
                        state = State::OutsideCodeBlock;
                    } else {
                        example.add_output(line);
                    }
                }
                State::InsideOther => {
                    if line == "```" {
                        state = State::OutsideCodeBlock;
                    }
                }
            },
            Err(e) => return Err(e),
        }
    }

    Ok(ret)
}
