fn main() {
    println!(
        "\
# Suux

Suux is an attempt to redesign common unix tools to work together
more consistently and flexibly.

See https://github.com/andybalaam/suux\
"
    );
}
