all: test

test:
	cargo fmt
	cargo test
	cargo build
	./target/debug/test-suux-docs

doc:
	rustup doc --std
	cargo doc --open
