# Suux

Suux is an attempt to redesign common Unix tools to work together more
consistently and flexibly.

Currently it consists of some ideas and early prototypes.

## Examples

The `list` command, unlike the original `ls`, is very simple.  It can only
print a list of all files in a directory.

```bash
$ list .
Cargo.toml
.git
.gitignore
Makefile
README.md
src
```

Fortunately, we can use `list-visible` to list only visible files:

```bash
$ list-visible .
Cargo.toml
Makefile
README.md
src
```

`list-visible` itself is actually implemented something like this:

```bash
list "$1" | only '^[^.]'
```

`only` is a filter (like original `grep`) that only allows files through if
they match the supplied regular expression.

To find out more about the files, we can process the output with `file-type`.

```bash
$ list-visible . | file-type
file-path	file-type	
Cargo.toml	file	
Makefile	file	
README.md	file	
src	dir	
```

```bash
$ list . | only-visible | file-type
file-path	file-type	
Cargo.toml	file	
Makefile	file	
README.md	file	
src	dir	
```

## Build and use

First, [install Rust](https://www.rust-lang.org/tools/install).

To build the code:

```bash
rustup component add rustfmt
cargo fmt
cargo build
cargo test
```

To use it, add the `bin` and `target/debug` directories to your PATH.  For
example, you could add a line like this to your `.bashrc` file or equivalent:

```bash
export PATH="/path/to/suux/bin|/path/to/suux/target/debug:$PATH"
```

## Philosophy

Suux core commands try to do a single job, to be consistent with each other,
and to have the simplest possible interfaces, so they are easy to remember
and flexible when interoperating.

Where original Unix commands have added more flags and options to support
complex use-cases, Suux tries to provide new commands to express new use-cases,
and tries to provide them in ways that can be re-used by multiple commands.

For example, in Suux, the equivalent of the original `find` command with
filters on filename or size would be to use `list-recursive` and filter results
with `only-field-compare` or `except-field-compare`, which filter results based
on column properties:

```bash
list-recursive . | file-size | only-field-compare "size" ">500"
```

(See "Performance" below for ideas on how we can make recursive searches
efficient by pruning branches early, even within this scheme.)

Suux also contains wrapper commands that have more complex interfaces that
are more convenient for people to use.  They are implemented by combining
core commands together.  Eventually, these wrappers may even provide a
POSIX-compatible command set.

## Pipeline data standard

Most commands in Suux emit and/or receive data in the Suux standard format.

The standard format exists in two forms:

1. A flat list with no title - each list item is a single line ending with
   a newline character.

2. Tab-separated values, where the first line provides headings.  Every line
   ends with a tab character, and every line contains the same number of
   columns, separated by tabs.

We can identify what format we are receiving by looking at the first line.
If it ends with a tab, we are using tab-separated values.  Otherwise, this is
a flat list.

Here is an example of a flat list:

```
hello
  worlds of consistency

?
```

And here is an example of tab-separated values with headings:

```
type	message	
greeting	hello	
body	  worlds of consistency	
body		
footer	?	
```

## Implementation

In its prototype form, many commands in Suux are actually bash scripts, but
as we proceed, more will be implemented in Rust.

## Performance

Stringing together many simple commands may hurt performance for two reasons:

1. Too many many processes and pipes, or
2. Filtering comes too late.

We will cover both these cases in the following sections.  Note that this
work is at the ideas stage for now, and is not implemented in the
currently-available commands.

### 1. Too many processes and pipes

If we must launch several commands to perform a task, we incur the overhead
of process creation as well as serialisation and deserialisation of our data
into the standard format.

In many cases, this overhead does not matter, but where it does we have a
partial solution: many commands in Suux will allow providing another Suux
command as an argument.  In some cases, these commands provided as arguments
will be used *as if* the provided commands followed this command in a pipe,
but actually the processing will happen within a single process.

For example:

```bash
list . '| only "^[A-Z]"'
```

will produce exactly the same output as:

```bash
list . | only "^[A-Z]"
```

but if we choose to implement it this way, we can code `list` to recognise
commands like `only` and provide their functionality within their own process
space.  This is optional, since a naive implementation of `list` can simply
execute the supplied command instead of recognising it.

### 2. Filtering comes too late

If we are performing a recursive search or similar, filtering after the entire
list has been generated may be far too inefficient, or incorrect, because
a filter should allow excluding an entire branch if it can be applied early.

For example, if we have a directory structure like this:

```bash
# Not working yet $ list-recursive .
B.txt
a/
hello.txt
Channels/
Channels/c.txt
```

Then to search for files and directories where the name starts with
a lowercase letter, we would do:

```bash
# Not working yet $ list-recursive . | file-name | only-field "file-name" "^[a-z]"
file-path	file-name	
a/	a	
hello.txt	hello.txt	
Channels/c.txt	c.txt	
```

But if we wanted to avoid searching inside directories with uppercase names,
this would not work, since `list-recursive` generates the whole list before
we apply our filters.

If instead we can provide the filters to `list-recursive` for it to apply
to every entry as it searches:

```bash
# Not working yet $ list-recursive . '| file-name' '| only-field "file-name" "^[a-z]"'
file-path	file-name	
a/	a	
hello.txt	hello.txt	
```

then we will get the result we wanted, because the `only-field` command is
run for every entry, and directories that do not match are removed without
looking inside them.

## License and credits

Copyright 2021 Andy Balaam and contributors, released under the
[AGPLv3 license](LICENSE) or later.

## Code of conduct

Please note that this project is released with a
[Contributor Code of Conduct](code_of_conduct.md).  By participating in this
project you agree to abide by its terms.

[![Contributor Covenant](contributor-covenant-v2.0-adopted-ff69b4.svg)](code_of_conduct.md)
